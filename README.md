Repository: [https://bitbucket.org/huanjason/sson](https://bitbucket.org/huanjason/sson)

Frontend: HTML5, CSS3, Bootstrap, Javascript, AngularJS

Backend: Python

Charting Library: Google Charts


.

1. Create a HTML template/layout using bootstrap & jQuery and/or angular JS (please refer to file - design_slide_1.png).

    - URL: [http://dgchart-hrd.appspot.com/static/sson/index.html](http://dgchart-hrd.appspot.com/static/sson/index.html)

    .

2. Write a script to convert Excel (sample_data.xlsx) data into JSON using PHP (or your preferred programming language).

    - Language: Python

    - Script: [data/xl2json.py](https://bitbucket.org/huanjason/sson/src/55686ed0a628ce330e03e54612f90dbbb9f5f2c3/data/xl2json.py?at=master&fileviewer=file-view-default)

    .

3. Using your template from instruction #1, create another slide (please refer to the design_slide_2.png). In this slide, you will need to create a column chart - using the JSON data you output from step #2. You can use highcharts (or any charting library you preferred).

    - URL: [http://dgchart-hrd.appspot.com/static/sson/index.html#!/2](http://dgchart-hrd.appspot.com/static/sson/index.html#!/2)

    .

4. Optional: You can have this JSON data call using normal script or an API/ajax call.

    - API: [http://dgchart-hrd.appspot.com/static/sson/data/sample_data.json](http://dgchart-hrd.appspot.com/static/sson/data/sample_data.json)
