var gulp   = require('gulp')
var concat = require('gulp-concat')
var uglify = require('gulp-uglify')
// var sourcemaps = require('gulp-sourcemaps')

gulp.task('js', function () {
  gulp.src( [ 'core/*.js', 'components/**/*.js' ] )
    // .pipe(sourcemaps.init())
    .pipe(concat('app.js'))
    //.pipe(uglify())
    //.pipe(sourcemaps.write())
    .pipe(gulp.dest('.'))
})

gulp.task( 'default', ['js'], function () {
  gulp.watch( [ 'core/*.js', 'components/**/*.js' ], ['js'] )
})

