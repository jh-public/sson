'use strict';
var myApp = angular.module( 'app', [
    ,'ui.router'
    ,'app.services'
]);

myApp.config([
    '$stateProvider'
    ,'$urlRouterProvider'
    ,function(
        $stateProvider
        ,$urlRouterProvider
    ){
        $stateProvider

        .state({
            name:       'home'
            ,url:       '/'
            ,component: 'home'
        })

        .state({
            name:       'home.page1'
            ,url:       '1'
            ,component: 'page1'
        })

        .state({
            name:       'home.page2'
            ,url:       '2'
            ,component: 'page2'
            ,resolve:   {
                data: 'dataService'
            }
        })

        $urlRouterProvider.otherwise( '/1' );
}]);
