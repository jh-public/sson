"use strict";
var app = angular.module( 'app.services', [] );


// Service to get data using AJAX
app.factory( 'dataService', [
        '$http'
    ,function(
        $http
    ){
        return $http.get( 'data/sample_data.json' )
        .then( function( response ){
            return response.data;
        });
    }
]);


// Colours used for charting
app.value( 'colors', [
     'red'
    ,'orange'
    ,'yellow'
    ,'green'
    ,'blue'
    ,'indigo'
    ,'violet'
]);


// Function to transform data into google chart format
app.factory( 'transformData', [
        'colors'
    ,function(
        colors
    ){
        return function( data ){
            var result = angular.copy( data.slice(0,1) );
            result[0].push( {role:'annotation'} );
            result[0].push( {role:'style'} );

            angular.forEach( data.slice(1), function( val, i ){
                result.push( [
                        val[0]
                    ,val[1] * 0.01
                    ,val[1] + '%'
                    ,'fill-color:' + colors[i]
                    ] );
            });

            return result;
        }
    }
]);


// Function to draw bar chart
app.factory( 'barChartFactory', [
    function(){

        return function( chartData, options ){

            function draw(){
                var data  = new google.visualization.arrayToDataTable( chartData );
                var chart = new google.visualization.ColumnChart(
                    document.getElementById( 'chart_div' )
                );
                chart.draw( data, options );
            }

            google.charts.load( 'current', { packages: [ 'corechart', 'bar' ] } );
            google.charts.setOnLoadCallback( draw );
        }
    }
]);
