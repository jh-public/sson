'use strict';
var myApp = angular.module( 'app', [
    ,'ui.router'
    ,'app.services'
]);

myApp.config([
    '$stateProvider'
    ,'$urlRouterProvider'
    ,function(
        $stateProvider
        ,$urlRouterProvider
    ){
        $stateProvider

        .state({
            name:       'home'
            ,url:       '/'
            ,component: 'home'
        })

        .state({
            name:       'home.page1'
            ,url:       '1'
            ,component: 'page1'
        })

        .state({
            name:       'home.page2'
            ,url:       '2'
            ,component: 'page2'
            ,resolve:   {
                data: 'dataService'
            }
        })

        $urlRouterProvider.otherwise( '/1' );
}]);

"use strict";
angular.module( 'filters', [] )

"use strict";
var app = angular.module( 'app.services', [] );


// Service to get data using AJAX
app.factory( 'dataService', [
        '$http'
    ,function(
        $http
    ){
        return $http.get( 'data/sample_data.json' )
        .then( function( response ){
            return response.data;
        });
    }
]);


// Colours used for charting
app.value( 'colors', [
     'red'
    ,'orange'
    ,'yellow'
    ,'green'
    ,'blue'
    ,'indigo'
    ,'violet'
]);


// Function to transform data into google chart format
app.factory( 'transformData', [
        'colors'
    ,function(
        colors
    ){
        return function( data ){
            var result = angular.copy( data.slice(0,1) );
            result[0].push( {role:'annotation'} );
            result[0].push( {role:'style'} );

            angular.forEach( data.slice(1), function( val, i ){
                result.push( [
                        val[0]
                    ,val[1] * 0.01
                    ,val[1] + '%'
                    ,'fill-color:' + colors[i]
                    ] );
            });

            return result;
        }
    }
]);


// Function to draw bar chart
app.factory( 'barChartFactory', [
    function(){

        return function( chartData, options ){

            function draw(){
                var data  = new google.visualization.arrayToDataTable( chartData );
                var chart = new google.visualization.ColumnChart(
                    document.getElementById( 'chart_div' )
                );
                chart.draw( data, options );
            }

            google.charts.load( 'current', { packages: [ 'corechart', 'bar' ] } );
            google.charts.setOnLoadCallback( draw );
        }
    }
]);

'use strict';
angular.module( 'app' )
.component( 'footer', {
    templateUrl: 'components/footer/footer.html'
});

'use strict';
angular.module( 'app' )
.component( 'header', {
    templateUrl: 'components/header/header.html'
});

'use strict';
angular.module( 'app' )
.component( 'home', {
    templateUrl: 'components/home/home.html'
});
'use strict';
angular.module( 'app' )
.component( 'navigationBar', {
    templateUrl: 'components/navigationBar/navigationBar.html'
    ,controller: [
            '$state'
        ,function(
            $state
        ){
            this.$onInit = function(){
                this.state = $state;
                this.links = [ // navigation bar information
                    {
                        state: 'home'
                        ,type: 'icon'
                        ,icon: 'glyphicon glyphicon-home'
                    }
                    ,{
                        state: 'home.page1'
                        ,type: 'text'
                        ,text: '1'
                    }
                    ,{
                        state: 'home.page2'
                        ,type: 'text'
                        ,text: '2'
                    }
                ];
            };
        }
    ]
});
'use strict';
angular.module( 'app' )
.component( 'page1', {
    templateUrl: 'components/page1/page1.html'
});

'use strict';
angular.module( 'app' )
.component( 'page2', {
    templateUrl: 'components/page2/page2.html'
    ,bindings: {
        data: '<'
    }
    ,controller: [
        function(){
            this.$onInit = function(){
                this.rightPaneHeight = ( window.innerHeight * 0.7 ) + 'px';
            };
        }
    ]
});

'use strict';
angular.module( 'app' )
.component( 'pageTitle', {
    templateUrl: 'components/pageTitle/pageTitle.html'
    ,bindings: {
        title: "@"
    }
});
"use strict";
angular.module( 'app' )
.component( 'barChart', {
    templateUrl: 'components/charts/barChart/barChart.html'
    ,bindings: {
        data: '<'
    }
    ,controller: [
             'barChartFactory'
            ,'transformData'
        ,function(
             barChartFactory
            ,transformData
        ){
            this.$onInit = function(){
                this.chartWidth     = ( window.innerWidth / 4 * 3 );
                this.chartHeight    = ( window.innerHeight * 0.7  );

                var options = { // charting options
                    title: 'Cras rhoncus laoreet semper'
                    ,titleTextStyle: {
                         fontSize: 20
                    }
                    ,vAxis: {
                        format: 'percent'
                    }
                    ,legend: {position: 'none'}
                    ,width:  this.chartWidth
                    ,height: this.chartHeight
                };

                // Transform raw data for google chart
                var transformedData = transformData( this.data );

                if ( !this.drawn ){
                    barChartFactory( transformedData, options );
                    this.drawn = true;
                }
            };
        }
    ]
})
