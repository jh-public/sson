'use strict';
angular.module( 'app' )
.component( 'navigationBar', {
    templateUrl: 'components/navigationBar/navigationBar.html'
    ,controller: [
            '$state'
        ,function(
            $state
        ){
            this.$onInit = function(){
                this.state = $state;
                this.links = [ // navigation bar information
                    {
                        state: 'home'
                        ,type: 'icon'
                        ,icon: 'glyphicon glyphicon-home'
                    }
                    ,{
                        state: 'home.page1'
                        ,type: 'text'
                        ,text: '1'
                    }
                    ,{
                        state: 'home.page2'
                        ,type: 'text'
                        ,text: '2'
                    }
                ];
            };
        }
    ]
});