"use strict";
angular.module( 'app' )
.component( 'barChart', {
    templateUrl: 'components/charts/barChart/barChart.html'
    ,bindings: {
        data: '<'
    }
    ,controller: [
             'barChartFactory'
            ,'transformData'
        ,function(
             barChartFactory
            ,transformData
        ){
            this.$onInit = function(){
                this.chartWidth     = ( window.innerWidth / 4 * 3 );
                this.chartHeight    = ( window.innerHeight * 0.7  );

                var options = { // charting options
                    title: 'Cras rhoncus laoreet semper'
                    ,titleTextStyle: {
                         fontSize: 20
                    }
                    ,vAxis: {
                        format: 'percent'
                    }
                    ,legend: {position: 'none'}
                    ,width:  this.chartWidth
                    ,height: this.chartHeight
                };

                // Transform raw data for google chart
                var transformedData = transformData( this.data );

                if ( !this.drawn ){
                    barChartFactory( transformedData, options );
                    this.drawn = true;
                }
            };
        }
    ]
})
