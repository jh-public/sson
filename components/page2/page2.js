'use strict';
angular.module( 'app' )
.component( 'page2', {
    templateUrl: 'components/page2/page2.html'
    ,bindings: {
        data: '<'
    }
    ,controller: [
        function(){
            this.$onInit = function(){
                this.rightPaneHeight = ( window.innerHeight * 0.7 ) + 'px';
            };
        }
    ]
});
