'use strict';
angular.module( 'app' )
.component( 'pageTitle', {
    templateUrl: 'components/pageTitle/pageTitle.html'
    ,bindings: {
        title: "@"
    }
});