#!/usr/bin/env python
import argparse
import json
import xlrd


def output_data( data, excel_filename ):
    output_filename = '%s.json' % excel_filename.split('.')[0]
    with open( output_filename, 'w' ) as f:
        json.dump( data, f )
    print( output_filename )


def xl2json( excel_filename ):
    workbook = xlrd.open_workbook( excel_filename )
    sheet = workbook.sheets()[0]
    data  = [
        [
            sheet.cell( row, col ).value
            for col in range( sheet.ncols )
        ]
        for row in range( sheet.nrows )
    ]
    output_data( data, excel_filename )


def get_args():
    parser = argparse.ArgumentParser()
    parser.add_argument( 'excel_filename' )
    return parser.parse_args()


if __name__ == '__main__':
    args = get_args()
    xl2json( args.excel_filename )
